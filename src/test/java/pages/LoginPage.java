package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class LoginPage extends BasePage {

    //Enter Elements here - Example provided below - Need to update to fit application
    @FindBy(id = "username")
    private WebElement inputUsername;
    @FindBy(id = "password")
    private WebElement inputPassword;
    @FindBy(className = "btn-primary")
    private WebElement btnLogin;

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void login() {
        wait.until(ExpectedConditions.visibilityOf(inputUsername));
        inputUsername.clear();
        inputUsername.sendKeys(config.getProperty("user"));
        inputPassword.clear();
        inputPassword.sendKeys(config.getProperty("password"));
        btnLogin.click();
    }

}
