package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class DragAndDrop extends BasePage {
    public DragAndDrop(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    private JavascriptExecutor executor = (JavascriptExecutor) driver;
    Actions action = new Actions(driver);

    static final String javascriptDragAndDrop =
            "var src=arguments[0],tgt=arguments[1];" +
                    "var dataTransfer={" +
                    "dropEffect:''," +
                    "effectAllowed:'all'," +
                    "files:[]," +
                    "items:{}," +
                    "types:[]," +
                    "setData:function(format,data){this.items[format]=data;this.types.append(format);}," +
                    "getData:function(format){return this.items[format];" +
                    "}," +
                    "clearData:function(format){}};" +
                    "var emit=function(event,target){var evt=document.createEvent('Event');" +
                    "evt.initEvent(event,true,false);" +
                    "evt.dataTransfer=dataTransfer;target.dispatchEvent(evt);};" +
                    "emit('dragstart',src);" +
                    "emit('dragenter',tgt);" +
                    "emit('dragover',tgt);" +
                    "emit('drop',tgt);" +
                    "emit('dragend',src);";


    public void dragAndDrop(WebElement dropFrom, WebElement dropTo) {

        executor.executeScript(javascriptDragAndDrop, new Object[]{dropFrom, dropTo});
        wait.until(ExpectedConditions.visibilityOf(dropTo));
    }

}
