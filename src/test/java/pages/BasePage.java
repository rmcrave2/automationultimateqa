package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BasePage {

    public WebDriverWait wait;
    FluentWait<WebDriver> fluentWait;
    protected static WebDriver driver;
    public static final String FILE_PATH_SEPARATOR = System.getProperty("file.separator");
    public static final String SYSTEM_USER_DIR = System.getProperty("user.dir");
    protected Properties config = getProperties("config.properties");
    static Properties property = new Properties();
    static FileInputStream fis;


    public BasePage(WebDriver driver) {
        this.driver = driver;
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 10);
        wait.pollingEvery(1, TimeUnit.SECONDS); // It should poll webelement after every single second
        wait.withTimeout(10, TimeUnit.SECONDS); // Max time for wait- If conditions are not met within this time frame then it will fail the script
        fluentWait = new FluentWait<WebDriver>(driver);
    }

    /**
     * Get properties from config.properties
     *
     * @param fileName
     * @return
     */
    public Properties getProperties(String fileName) {
        try {
            fis = new FileInputStream(SYSTEM_USER_DIR +
                    FILE_PATH_SEPARATOR + "configuration" +
                    FILE_PATH_SEPARATOR + fileName);
        } catch (FileNotFoundException e) {
            // log.error(e.getMessage());
            System.out.println("config.properties file not found");
        }
        try {
            property.load(fis);
        } catch (IOException e) {
            //log.error(e.getMessage());
            System.out.println("Error reading from config.properties file");
        }
        return property;
    }
}
