package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.w3c.dom.Element;

import java.util.List;

public class FillOutFormPage extends BasePage {

    //FindBy Elements go here
    @FindBy(id = "et_pb_contact_name_0")
    private WebElement firstContactName;
    @FindBy(id = "et_pb_contact_name_1")
    private WebElement secondContactName;
    @FindBy(id = "et_pb_contact_message_0")
    private WebElement firstContactMessage;
    @FindBy(id = "et_pb_contact_message_1")
    private WebElement secondContactMessage;
    @FindBy(className = "et_pb_contact_submit")
    private List<WebElement> submit;
    @FindBy(id = "et_pb_contact_form_0")
    private WebElement contactMessage1;
    @FindBy(id = "et_pb_contact_form_1")
    private WebElement contactMessage2;
    @FindBy(className = "et_pb_contact_captcha_question")
    private WebElement captchaText;
    @FindBy(className = "et_pb_contact_captcha")
    private WebElement captchaInput;

    public FillOutFormPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @Step("Returns a particular element")
    public WebElement getElement(String elementName) {
        switch (elementName) {
            case "Name1":
                return firstContactName;
            case "Name2":
                return secondContactName;
            case "Message1":
                return firstContactMessage;
            case "Message2":
                return secondContactMessage;
            case "Submit1":
                return submit.get(0);
            case "Submit2":
                return submit.get(1);
            default:
                return null;

        }
    }

    @Step("Fills out text field by passing name of text field to change")
    public void fillOutTextField(String whichTextField, String fieldInput) {
        WebElement textFieldToChange = getElement(whichTextField);
        wait.until(ExpectedConditions.elementToBeClickable(textFieldToChange));
        textFieldToChange.click();
        textFieldToChange.sendKeys(fieldInput);
    }


    @Step("Clicks submit button based on variable being passed in")
    public String clickWhichSubmit(String whichSubmit) {
        WebElement submitToClick = getElement(whichSubmit);
        wait.until(ExpectedConditions.elementToBeClickable(submitToClick));
        submitToClick.click();
        if(whichSubmit.equalsIgnoreCase("Submit1")) {
            wait.until(ExpectedConditions.textToBePresentInElement(contactMessage1, "Form filled out successfully"));
            return contactMessage1.getText();
        }
        else
        {
            wait.until(ExpectedConditions.textToBePresentInElement(contactMessage2, "Success"));
            return contactMessage2.getText();
        }

    }

    @Step("Gets captcha numbers, adds, then enters captcha")
    public void fillOutCaptcha() {
        String[] capcthaNumbers = captchaText.getText().split("\\+");
        capcthaNumbers[0] = capcthaNumbers[0].trim();
        capcthaNumbers[1] = capcthaNumbers[1].trim();
        String captchaValue = String.valueOf((Integer.parseInt(capcthaNumbers[0])) + (Integer.parseInt(capcthaNumbers[1])));
        wait.until(ExpectedConditions.elementToBeClickable(captchaInput));
        captchaInput.click();
        captchaInput.sendKeys(captchaValue);
    }

    @Step("Gets pop up message and returns missing items")
    public String negativeTestingSubmit1() {
        submit.get(0).click();
        String popupMessage = contactMessage1.findElement(By.className("et-pb-contact-message")).findElement(By.tagName("ul")).getText();
        popupMessage = popupMessage.replace("\n", " ").replace("\r", " ");
        return popupMessage;
    }

    @Step("Gets pop up message and returns missing items")
    public String negativeTestingSubmit2() {
        submit.get(1).click();
        wait.until(ExpectedConditions.textToBePresentInElement(contactMessage2, "Please, fill in the following fields:"));
        String popupMessage = contactMessage2.findElement(By.className("et-pb-contact-message")).findElement(By.tagName("ul")).getText();
        popupMessage = popupMessage.replace("\n", " ").replace("\r", " ");
        return popupMessage;
    }

    @Step("Clears out text field by passing name of text field to clear")
    public void clearTextField(String whichTextField ) {
        WebElement fieldToClear = getElement(whichTextField);
        wait.until(ExpectedConditions.elementToBeClickable(fieldToClear));
        fieldToClear.click();
        fieldToClear.clear();
    }
}
