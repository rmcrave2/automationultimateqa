package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class LinkSelectPage extends BasePage {

    //FindBy Elements go here
    @FindBy(id = "menu-item-587")
    private WebElement automationExercises;

    public LinkSelectPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @Step("Clicks link by given text name and verifies page loaded by expected URL Text")
    public void clickLinkByName(String linkName) {
        WebElement linkByText = driver.findElement(By.linkText(linkName));
        wait.until(ExpectedConditions.elementToBeClickable(linkByText));
        linkByText.click();
        wait.until(ExpectedConditions.urlContains(urlText(linkName)));
    }

    @Step("Returns text from expected URL")
    public String urlText(String linkName) {
        switch (linkName) {
            case "Fill out forms":
                return "filling-out-forms";
            case "Automation Exercises":
                return "automation";
            default:
                return "null";

        }
    }

}
