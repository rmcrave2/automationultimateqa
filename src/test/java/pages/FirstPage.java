package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class FirstPage extends BasePage {

    @FindBy(id="s") private WebElement searchField;
    @FindBy(id="searchsubmit") private WebElement searchSubmitButton;


    public FirstPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);

    }

    public void enterTextIntoWebSearch(String textToEnter){
        wait.until(ExpectedConditions.visibilityOf(searchField));
        searchField.sendKeys(textToEnter);
    }

    public void clickWebSearch(){
        wait.until(ExpectedConditions.visibilityOf(searchSubmitButton));
        searchSubmitButton.click();
    }

}
