package tests;

import listeners.TestListener;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import pages.FillOutFormPage;
import pages.LinkSelectPage;

@Listeners({TestListener.class})
public class FormsTest extends BaseTest {

    @Parameters({"browsertype", "testURL"})
    @BeforeClass(alwaysRun = true)
    public void beforeEverything(@Optional("Chrome") String browsertype, @Optional("https://www.ultimateqa.com/automation/") String testURL) {
        setup(testURL, browsertype);
        lsp = new LinkSelectPage(driver);
        fofp = new FillOutFormPage(driver);
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {
        softAssert = new SoftAssert();
    }

    @Test(groups = {"Forms"}, priority = 0)
    public void PositiveFormTestNonCaptcha() {
        lsp.clickLinkByName("Fill out forms");
        fofp.fillOutTextField("Name1", "Billy Bob");
        fofp.fillOutTextField("Message1", "Hello, World");
        softAssert.assertEquals(fofp.clickWhichSubmit("Submit1"), "Form filled out successfully");
        softAssert.assertAll();
    }

    @Test(groups = {"Forms"}, priority = 1)
    public void PositiveFormTestCaptcha() {
        lsp.clickLinkByName("Automation Exercises");
        lsp.clickLinkByName("Fill out forms");
        fofp.fillOutTextField("Name2", "Automation Testing");
        fofp.fillOutTextField("Message2", "Automated messaging");
        fofp.fillOutCaptcha();
        softAssert.assertEquals(fofp.clickWhichSubmit("Submit2"), "Success");
        softAssert.assertAll();
    }

    @Test(groups = {"Forms"}, priority = 2)
    public void NegativeFormTestNonCaptcha() {
        lsp.clickLinkByName("Automation Exercises");
        lsp.clickLinkByName("Fill out forms");
        softAssert.assertEquals(fofp.negativeTestingSubmit1(), "Name Message");
        fofp.fillOutTextField("Name1","Testing");
        softAssert.assertEquals(fofp.negativeTestingSubmit1(), "Message");
        fofp.clearTextField("Name1");
        fofp.fillOutTextField("Message1","Automation Testing");
        softAssert.assertEquals(fofp.negativeTestingSubmit1(), "Name");
        softAssert.assertAll();
    }

    @Test(groups = {"Forms"}, priority = 3)
    public void NegativeFormTestCaptcha() {
        lsp.clickLinkByName("Automation Exercises");
        lsp.clickLinkByName("Fill out forms");
        softAssert.assertEquals(fofp.negativeTestingSubmit2(), "Name Message Captcha");
        fofp.fillOutTextField("Name2","Testing");
        softAssert.assertEquals(fofp.negativeTestingSubmit2(), "Message Captcha");
        fofp.clearTextField("Name2");
        fofp.fillOutTextField("Message2","Automation Testing");
        softAssert.assertEquals(fofp.negativeTestingSubmit2(), "Name Captcha");
        fofp.fillOutTextField("Name2","Testing");
        softAssert.assertEquals(fofp.negativeTestingSubmit2(), "Captcha");
        fofp.clearTextField("Name2");
        fofp.clearTextField("Message2");
        fofp.fillOutCaptcha();
        softAssert.assertEquals(fofp.negativeTestingSubmit2(), "Name Message");
        fofp.fillOutTextField("Name2","Testing");
        softAssert.assertEquals(fofp.negativeTestingSubmit2(), "Message");
        fofp.clearTextField("Name2");
        fofp.fillOutTextField("Message2","Automation Testing");
        softAssert.assertEquals(fofp.negativeTestingSubmit2(), "Name");
        softAssert.assertAll();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        driver.quit();
    }

}
