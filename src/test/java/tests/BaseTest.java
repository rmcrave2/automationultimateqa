package tests;

import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.asserts.*;
import pages.FillOutFormPage;
import pages.FirstPage;
import pages.LinkSelectPage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Properties;

public class BaseTest {


    public static WebDriver driver;
    protected static String baseUrl;
    protected SoftAssert softAssert = new SoftAssert();

    //these are the starting pages used for testing
    protected static FirstPage fp;
    protected static LinkSelectPage lsp;
    protected static FillOutFormPage fofp;

    @BeforeClass
    public void setup(String url, String browsertype) {
        baseUrl = url;
        try {
            createDriver(browsertype, baseUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void createDriver(String browsertype, String url) throws IOException {
        String myFile = new java.io.File("").getAbsolutePath();
        myFile = myFile.concat("\\src\\test\\java\\drivers\\");
        myFile = myFile.replace("\\", "\\\\");
        Properties config = new Properties();
        try {
            InputStream input = new FileInputStream("configuration/config.properties");
            config.load(input);
            System.out.println("Connect to local VM: " + config.getProperty("localVM"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // connects to the VM for local testing
        if (config.getProperty("localVM").toLowerCase().contains("true")) {
            String ipAddress = "http://" + config.getProperty("localVMip") + ":4444/wd/hub";
            System.out.println(ipAddress);
            if (browsertype.contains("Chrome")) {
                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("excludeSwitches", Arrays.asList("enable-automation"));
                options.addArguments("chrome.switches", "--disable-extensions --disable-extensions-file-access-check --disable-extensions-http-throttling");
                DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                System.setProperty("webdriver.chrome.driver", myFile + "chromedriver.exe");
                driver = new RemoteWebDriver(new URL(ipAddress), capabilities);
            } else if (browsertype.contains("IE")) {
                System.setProperty("webdriver.ie.driver", myFile + "IEDriverServer.exe");
                DesiredCapabilities ieCaps = DesiredCapabilities.internetExplorer();
                driver = new RemoteWebDriver(new URL(ipAddress), ieCaps);
            } else if (browsertype.contains("Firefox")) {
                System.setProperty("webdriver.gecko.driver", myFile + "geckodriver.exe");
                System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");
                FirefoxProfile firefoxProfile = new FirefoxProfile();
                DesiredCapabilities dc = new FirefoxOptions().setProfile(firefoxProfile).addTo(DesiredCapabilities.firefox());
                driver = new RemoteWebDriver(new URL(ipAddress), dc);
            } else {
                Reporter.log("Error. Invalid BrowserType", true);
                throw new RuntimeException(); // skips tests with invalid browser type
            }
        } else {
            // run locally on the computer, needed for the nightly builds
            if (browsertype.contains("Chrome")) {
                ChromeOptions options = new ChromeOptions();
                //options to disable chrome notifications about automation/extensions running.
                options.setExperimentalOption("excludeSwitches", Arrays.asList("enable-automation"));
                options.addArguments("chrome.switches", "--disable-extensions --disable-extensions-file-access-check --disable-extensions-http-throttling");
                DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                System.setProperty("webdriver.chrome.driver", myFile + "chromedriver.exe");
                driver = new ChromeDriver(capabilities);
            } else if (browsertype.contains("IE")) {
                System.setProperty("webdriver.ie.driver", myFile + "IEDriverServer.exe");
                driver = new InternetExplorerDriver();
            } else if (browsertype.contains("Firefox")) {
                System.setProperty("webdriver.gecko.driver", myFile + "geckodriver.exe");
                System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");
                FirefoxProfile firefoxProfile = new FirefoxProfile();
                firefoxProfile.setPreference("browser.startup.page", 0); // Empty start page
                firefoxProfile.setPreference("browser.startup.homepage_override.mstone", "ignore"); // Suppress the "What's new" page
                DesiredCapabilities dc = new FirefoxOptions().setProfile(firefoxProfile).addTo(DesiredCapabilities.firefox());
                driver = new FirefoxDriver(dc);
            } else {
                Reporter.log("Error. Invalid BrowserType", true);
                throw new RuntimeException(); // skips tests with invalid browser type
            }
        }


        System.out.println("=====Configure driver=====\n");
        driver.get(url);
        driver.manage().window().maximize();
        System.out.println("=====Application Started=====\n");
        System.out.println("Browsertype: " + browsertype + "\n");
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult result) {

        if (result.getStatus() == ITestResult.SUCCESS) {

            //Do something here
            System.out.println(result.getMethod().getMethodName() + ": Pass");
        }
        if (result.getThrowable() != null) {
            System.out.println(result.getMethod().getMethodName() + ": " + result.getThrowable());
        }
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

}
