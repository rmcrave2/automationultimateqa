package tests;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryTest implements IRetryAnalyzer {

    int counter = 0;
    int retryLimit = 2;

    //how to use, add ", retryAnalyzer = tests.RetryTest.class" at the end of each @test you want to rerun a failed test.
    @Override
    public boolean retry(ITestResult result) {

        if (counter < retryLimit) {
            counter++;
            System.out.print("Retry test" + "\n");
            return true;
        }
        return false;
    }
}