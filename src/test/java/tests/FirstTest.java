package tests;

import listeners.TestListener;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import pages.FirstPage;

@Listeners({TestListener.class})
public class FirstTest extends BaseTest {

    @Parameters({"browsertype","testURL"})
    @BeforeClass(alwaysRun = true)
    public void beforeEverything(@Optional("Chrome")String browsertype, @Optional("https://www.ultimateqa.com/automation/")String testURL){
        setup(testURL, browsertype);
        fp = new FirstPage(driver);

    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(){
        softAssert = new SoftAssert();
    }


    @Test(groups={"example"})
    public void Test_Case_Name(){
        fp.enterTextIntoWebSearch("Hello World");
        fp.clickWebSearch();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        driver.quit();
    }
}
