# Automation to Ultimate QA
This project is using java, selenium, and allure


### Prerequisite

1. IntelliJ IDE Community Edition https://www.jetbrains.com/idea/download/#section=windows
2. JDK 1.8 http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html 
3. Git for windows https://git-scm.com/download/win
4. GitLab account and create ssh key
5. Install scoop and allure

### Install Scoop (for windows)

* https://docs.qameta.io/allure/ for documentation if not using windows
1. Open PowerShell 3 and run the following command:  iex (new-object net.webclient).downloadstring('https://get.scoop.sh')
* Note: if you get an error you might need to change the execution policy (i.e. enable Powershell) with:  Set-ExecutionPolicy RemoteSigned -scope CurrentUser

### Installing Allure

1. After scoop is installed, type this command in PowerShell:   scoop install allure
2. To update allure, enter the this command in PowerShell:      scoop update allure
3. To check the version of allure, enter this command:          allure --version

### Setup

1. Create folder location for the git files to be stored
2. Right click the new folder and select git bash
3. git clone "project ssh" Note: Provided by gitlab
4. Open IntelliJ and import project, select as a Maven Project, and JDK 1.8 (windows> program files>java>1.8)


### How to use git

IntelliJ provides a terminal so you don't need to use Git Bash
Cheat sheet for git
https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf

Commands you will use
* git status # see the file changes compared to the branch
* git add # add a new file into the git
* git pull #always before a push. You want to grab all file updates on the branch before a push
* git commit “file name” -m “comments” # -m means you’re committing a file and leaving a message for the file change. Can be very short, ex: “nav bar find element update"
* git push # sends all the files committed into the branch
* git branch # see what branch you are in
* git branch -a # see all the available branches you can access
* git checkout branch name # do not include the branch path


### Creating branches

* Create branches with the terminal
* Master branch (no one merges)
* Dev Branch pull and create a new branch named the ticket #
* Start on the dev branch
* git checkout -b "ExampleBranch" #creates a new branch off the current branch you're on
* git commit files -m ""
* git fetch # updates the commits from all of the branches, if everything is up-to-date, you will see a blank return
* git merge origin/develop # pulls recent changes on develop to your local. Resolve the conflicts on your local branch before merging back to the develop. Up-to-date means no conflicts
* git push # do this when you're finished with all changes to get the terminal command to check the branch to remote/origin
* git push --set-upstream origin ExampleBranch # the terminal will give you this command. copy/paste it
* git branch -d # deletes a branch
* Go to the gitlab and request merge from ExampleBranch and change from master to dev branch.
* Approve and merge changes
* Ask for help if you run into merge conflicts, if you check in conflicts, the entire project will not work

### Git convention
You should git pull and check before adding or committing any file. If anyone else modified the file, there will be a message on the terminal noting there will be a conflict with files. Please try and resolve this issue before attempting to check in new work. Otherwise, it will override the previous file and only use your latest commited file.
Test your work before checking files.
Jenkins runs as a maven project. Before committing, you need to clean and test


### Project structure using Page Object Model
1. Drivers (selenium\src\test\java\drivers)
Contains chrome, firefox, and IE browsers
2. Pages (selenium\src\test\java\pages)
* File Naming convention: End with Page example. ExamplePage, BasePage.
* Element Naming convention: btn_submit, input_form
3. Suites
Jenkins will run the smoketest files. default @tests with the phrases smoketest
4. Tests
selenium\src\test\java\tests\BaseTest.java

### Generating allure report after test run
1. Open folder that has allure-results folder
* Ex: D:\Automation_Framework_Template\selenium
2. Right click and click Git Bash Here
3. Enter the following command allure serve allure-results
4. This will open up HTML report for the reruns in the allure-results folder